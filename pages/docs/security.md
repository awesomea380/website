# Report a Security Issue

If you want to report a security issue, open a new issue inside one of our
[GitLab repositories](https://gitlab.com/librewolf-community/browser). Please
make sure you pick the correct repository for you OS.

Also, when creating a new issue, be sure to
[mark it as confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html).
